import axios from "axios"

const API_URL  ='http://localhost:8080'

const IMAGE_URL  ='http://www.adtagsantamaria.com.br/images/im'

const KEY = 'AIzaSyDvQC0Z_ruKHhFB9fLlp7gERgUst0BIvP8'

const CANAL = 'UCXcE4VZs1M-TzoAFOnHiLcg'


export function fechIgrejas(){
    return axios(`${API_URL}/adtag/api/igreja/all`);

}


export function fechIgreja(id:number){
    return axios(`${API_URL}/adtag/api/igreja/${id}`);

}


export function fechEventos(){
    return axios(`${API_URL}/adtag/api/evento/all`);

}

export function getImageURL(name:string){
return `${IMAGE_URL}/${name}`
}


export function getUrlYoutube(){
    return `https://www.googleapis.com/youtube/v3/search?key=${KEY}&channelId=${CANAL}&part=snippet,id&order=date&maxResults=20`;
}

export function fechYoutube(){
    return axios(getUrlYoutube());
}
