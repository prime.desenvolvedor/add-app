import  React, { useEffect, useState } from "react";
import { RouteProp, useRoute } from "@react-navigation/native";
import { Text } from "react-native";
import { EventoParamsList } from "../../types";


type NavigationParams = RouteProp<EventoParamsList, 'EventoDetailEdit'>;

const EventoDetailEdit=({navigation}:any)=> {

    const {evt} = useRoute<NavigationParams>().params??{};

return (
    <>
    <Text>{evt.descricao}</Text>
    <Text>{evt.data}</Text>
    <Text>{evt.responsavel}</Text>
    <Text>{evt.resumo}</Text>
    <Text>{evt.banner1}</Text>
    </>    
);

}

export default EventoDetailEdit;