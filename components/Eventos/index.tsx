import  React, { useEffect, useState } from "react";
import { Alert, StyleSheet, Text, View, Image } from "react-native";
import { RectButton, ScrollView } from "react-native-gesture-handler";
import { fechEventos } from "../../Api";
import { Evento } from "../../types";


function Eventos({navigation}:any){


    const [eventos, setEventos] = useState<Evento[]>([]);

    const [isLoading, setIsloading] = useState(false);


    useEffect(()=>{
        setIsloading(true);
        fechEventos()
        .then(response => setEventos(response.data))
        .catch(error => Alert.alert('Algo saiu errado!!!!'))
        .finally(()=>setIsloading(false))
    }, []);
    

    return (
        <>

        
        <ScrollView>
            {isLoading ? (
                <Text>Carregando...</Text>  
            ) : (

                eventos.map((item:any) => (
                     <>   
                    <View key={item.id} style={styles.container}>
                   
                   <Text>{item.descricao}</Text>
                    <Text>{item.departamento.descricao}</Text>


                    </View>  
                     <View >
                     <RectButton onPress={()=> navigation.navigate( 'EventoDetail', {evt:item} )}>Saiba mais</RectButton>
                 </View>
                    </>



                )
                )
            )}



        </ScrollView>
        


        </>
    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 15,
        marginRight: 15,
    },
    imgvd:{
        width: 320,
        height: 180,   
    }


});

export default Eventos;