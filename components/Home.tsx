import React, { useState } from "react";
import { useEffect } from "react";
import { StyleSheet, Text, View, Image } from "react-native";

import { Igreja } from "../types";
import { fechIgreja } from "../Api";


function Home() {


  const [igreja, setIgreja] = useState<Igreja>({id:0,descricao:"",dirigente:"",telefone:"",email:"",endereco:"",numero:"",bairro:"",cidade:"",uf:""});

  const [isLoading, setIsLoading] = useState(false);

  useEffect(
    () => {
      setIsLoading(true);
      fechIgreja(1)
      .then(response =>setIgreja(response.data))
      .catch(error => console.log(error))
      .finally(() => setIsLoading(false));
    }, []
  );

  return (
    <>
   
    <View style={styles.container}>
      <Text style={styles.text}>Bem Vindo</Text>

      <Text style={styles.text}>{igreja.descricao}</Text>

      <Image style={styles.imgLogo} source={require('../assets/images/adtag.png')}  ></Image>

      <Text style={styles.text1}>{igreja.dirigente}</Text>

      <Text style={styles.text1}>{igreja.endereco}</Text>

      <Text style={styles.text1}>{igreja.bairro}</Text>

      <Text style={styles.text1}>{igreja.cidade}-{igreja.uf}</Text>

      <Text style={styles.text1}>{igreja.email}</Text>

      <Text style={styles.text1}>{igreja.telefone}</Text>


      
      
    </View>
   
    </>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignContent: 'center',
        paddingRight:15,
        paddingLeft:15,
    },
    text: {
        fontSize: 20,
        color: '#928C8C',
        textAlign: 'center',
        textTransform: 'uppercase',
        marginTop: 10,
    },
    text1: {
      fontSize:14,
      color: '#928C8C',
      textAlign: 'center',
    },
    imgLogo: {
      width: '100%',
      height: "200px",
      marginTop: 10,
      marginBottom: 10,
     
    }
    
});

export default Home;
