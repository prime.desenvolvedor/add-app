import  React, { useEffect, useState } from "react";
import { Alert, StyleSheet,TouchableOpacity,Text, View, Image, Linking } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { fechYoutube } from "../Api";
import { Vdyoutube } from "../types";


function Videos({navigation}:any){


    const [video, setVideo] = useState<Vdyoutube>();

    const [isLoading, setIsloading] = useState(false);


    useEffect(()=>{
        setIsloading(true);
        fechYoutube()
        .then(response => setVideo(response.data))
        .catch(error => Alert.alert('Algo saiu errado!!!!'))
        .finally(()=>setIsloading(false))
    }, []);
    

    return (
        <>
   <ScrollView>
            {isLoading ? (
                <Text>Carregando...</Text>  
            ) : (

                video?.items.map((item:any) => (
                   
                    <View key={item.id.videoId}  style={styles.container}>
                    <Image style={styles.imgvd} source={{uri:item.snippet.thumbnails.medium.url}} ></Image>

                    <Text onPress={() => Linking.openURL('https://www.youtube.com/watch?v='+item.id.videoId)}>{item.snippet.title}</Text>
                    
                    
                    </View>    
                   


                )
                )
            )}



        </ScrollView>
        


        </>
    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginLeft: 15,
        marginRight: 15,
    },
    imgvd:{
        width: 320,
        height: 180,   
    }


});

export default Videos;