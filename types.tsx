/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

import { BottomTabScreenProps } from '@react-navigation/bottom-tabs';
import { CompositeScreenProps, NavigatorScreenParams } from '@react-navigation/native';
import { NativeStackScreenProps } from '@react-navigation/native-stack';

declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList {}
  }
}

export type RootStackParamList = {
  Root: NavigatorScreenParams<RootTabParamList> | undefined;
  Modal: undefined;
  NotFound: undefined;
  Canal: undefined;
  EventoDetail:undefined;
  Home:undefined;
  Igreja:undefined;
};

export type RootStackScreenProps<Screen extends keyof RootStackParamList> = NativeStackScreenProps<
  RootStackParamList,
  Screen
>;

export type RootTabParamList = {
  TabOne: undefined;
  TabTwo: undefined;
  Canal: undefined;
};

export type RootTabScreenProps<Screen extends keyof RootTabParamList> = CompositeScreenProps<
  BottomTabScreenProps<RootTabParamList, Screen>,
  NativeStackScreenProps<RootStackParamList>
>;


export type Videos ={

  id:number;
  ativo:number;
  descricao:String;
  data:string;

}

export type Igreja={

   id:number;
 
 descricao:string;
 
 dirigente:string;
 
 telefone:string;
 
 email:string;
 
 endereco:string;
 
 numero:string;
 
 bairro:string; 
 
 cidade:string;
 
 uf:string;
}


export type Aviso={
   id:number;
 
 DataAviso:string;

 FimDataAviso:string;

 titulo:string;
 
 descricao:string;
 
 foto:string;
}

export type Departamento={

   id:number;
 
 descricao:string;
 
 resumo:string;
 
 lider:string;
 
 igreja:Igreja;
 
 ativo:number;
 
}

export type Evento={
   id:number;

 departamento:Departamento;
 
 descricao:string;
 
   data:string;
 
 responsavel:string;
 
 resumo:string;
 
 banner1:string;
 
 banner2:string;
 
 ativo:number;

}

export type EventoParamsList = {
 EventoDetailEdit:{
   evt:Evento;
 }

 }


 export type Vdyoutube={
 
   "kind": string,
   "etag": string,
   "nextPageToken": string,
   "regionCode": string,
   "pageInfo": {
     "totalResults": string,
     "resultsPerPage": string
   },
   "items": [
     {
       "kind": string,
       "etag": string,
       "id": {
         "kind": string,
         "videoId": string
       },
       "snippet": {
         "publishedAt": string,
         "channelId": string,
         "title": string,
         "description": string,
         "thumbnails": {
           "default": {
             "url": string,
             "width": number,
             "height": number
           },
           "medium": {
             "url": string,
             "width": number,
             "height": number
           },
           "high": {
             "url": string,
             "width": number,
             "height": number
           }
         },
         "channelTitle": string,
         "liveBroadcastContent": string,
         "publishTime": string
       }
     }
     ]
     
}

